---
title: Aperture Neuro
permalink: /index.html
layout: home.njk
class: home
---


The purpose of Aperture is to enable a diverse approach to sharing and communicating high quality, community-based, open neuroscience while bringing transparency and interactivity to the publishing process.